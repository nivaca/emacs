(use-package company
  :commands global-company-mode
  ;; :bind ("C-." . company-complete)
  :init (progn
          (global-company-mode)
          (setq company-global-modes '(not python-mode cython-mode sage-mode))
          )
  :config (progn
            (setq company-tooltip-limit 20 ; bigger popup window
                  company-idle-delay 2    ; decrease delay before autocompletion popup shows
                  company-echo-delay 0     ; remove annoying blinking
                  company-begin-commands '(self-insert-command) ; start autocompletion only after typing
                  company-transformers '(company-sort-by-occurrence)
                  company-selection-wrap-around t
                  )
                  (bind-keys :map company-active-map
                             ("C-n" . company-select-next)
                             ("C-p" . company-select-previous)
                             ("C-d" . company-show-doc-buffer)
                             ("<tab>" . company-complete))
            )
  )

(use-package company-auctex
  :ensure t
  ; :config (company-auctex-init)
  )



(provide 'mycompany)
