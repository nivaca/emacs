;; My init file

(eval-when-compile (require 'cl)) ;; Require Common Lisp (for "case" etc.)


(case system-type
  ('gnu/linux
   (setq user-emacs-directory "~/Dropbox/emacs_x/")
   )
  ('darwin
    (progn
     (setq user-emacs-directory "~/Dropbox/emacs/")
     (setq delete-by-moving-to-trash t)
      (defun system-move-file-to-trash (file)
        "Use \"trash\" to move FILE to the system trash.
      When using Homebrew, install it using \"brew install trash\"."
        (call-process (executable-find "trash")
          nil 0 nil
          file))
    )
  )
)



(let ((default-directory user-emacs-directory))
  (normal-top-level-add-subdirs-to-load-path)
  )


(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setq exec-path (append exec-path '("/usr/local/bin")))




; ------------------------------------------------------------
;;; Turn off the annoying crap immediately
(setq backup-inhibited t
      auto-save-default t
      inhibit-startup-message t
      dabbrev-case-distinction nil
      dabbrev-case-fold-search nil
      vc-follow-symlinks t ;; Follow symbolic links
      echo-keystrokes 0.1
      disabled-command-function nil
      large-file-warning-threshold 536870911)



;; Remove text in active region if inserting text
(delete-selection-mode 1)

;; Don't highlight matches with jump-char - it's distracting
(setq jump-char-lazy-highlight-face nil)

;; Never insert tabs
(set-default 'indent-tabs-mode nil)

;; Easily navigate sillycased words
(global-subword-mode 1)

;; Don't break lines for me, please
(setq-default truncate-lines t)

;; Allow recursive minibuffers
(setq enable-recursive-minibuffers t)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq message-log-max t)

;; Share clipboard
(setq x-select-enable-clipboard t)




;; ==================== UTF-8 =======================
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8)
;; From Emacs wiki
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))



;; ================= Package Management ===============
(load-file (concat user-emacs-directory "mypackages.el"))


; ------------------------------------------------------------
;;;; Modes and mode groupings
(defmacro hook-into-modes (func modes)
  "Add hook `FUNC' to multiple `MODES'."
  `(dolist (mode-hook ,modes)
  (add-hook mode-hook ,func)))


;; ==================== Server ====================
(if (not server-mode)
    (server-start nil t))


;; ================ My display configuration =============
(load-file (concat user-emacs-directory "mydisplay.el"))


;; ===================== AUCTEX =========================
(load-file (concat user-emacs-directory "myauctexconf.el"))


;; ========== Disable native GIT support ==============
(setq vc-handled-backends nil)
(setq vc-handled-backends ())




;; ================= FLYSPELL ============================
(use-package flyspell
  :ensure t
  :config
  (progn
    (define-key flyspell-mode-map [down-mouse-3] 'flyspell-correct-word)
    (if (eq system-type 'darwin)
        (setq ispell-program-name "aspell"
              aspell-dictionary "en_GB-ise-wo_accents"
              aspell-program-name "/usr/local/bin/aspell"
              ispell-dictionary "en_GB-ise-wo_accents"
              ispell-program-name "/usr/local/bin/aspell")
      )
    (if (eq system-type 'gnu/linux)
      (progn
        (setq ispell-program-name "aspell"
              aspell-dictionary "en_GB-ise-wo_accents"
              aspell-program-name "/usr/bin/aspell"
              ispell-dictionary "en_GB-ise-wo_accents"
              ispell-program-name "/usr/bin/aspell")
                    (unbind-key "C-." flyspell-mode-map)
                    (unbind-key "C-," flyspell-mode-map)
                   (bind-key "C-." 'comment-or-uncomment-line-or-region)
                   (bind-key "C-," 'comment-or-uncomment-line-or-region)
        )
      )
    (defun turn-on-flyspell ()
      "Force flyspell-mode on using a positive arg.  For use in hooks."
      (interactive)
      (flyspell-mode 1))
    (add-hook 'LaTeX-mode-hook 'flyspell-mode)
    (add-hook 'message-mode-hook 'turn-on-flyspell)
    ;; (add-hook 'text-mode-hook 'turn-on-flyspell)
    )
  )




;; ===============  Yasnippet  ===============
(use-package yasnippet
  :if (not noninteractive)
  :diminish yas-minor-mode
  :commands (yas-global-mode yas-minor-mode)
  :ensure t
  :config
  (progn
    (setq
      yas-indent-line nil
      (case system-type
        ('gnu/linux
          (setq yas-snippet-dirs (append yas-snippet-dirs
             '("~/Dropbox/emacs_x/snippets")))
        )
        ('darwin
          (setq yas-snippet-dirs (append yas-snippet-dirs
             '("~/Dropbox/emacs/snippets")))
        )
      )
    )
  )
)



;; ============= Commenting ===============
(defun comment-or-uncomment-line-or-region ()
  "Comments or uncomments the current line or region."
  (interactive)
  (if (region-active-p)
      (comment-or-uncomment-region (region-beginning) (region-end))
    (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    )
  )


;; =================  parentheses ================
(load-file (concat user-emacs-directory "myparent.el"))


;; =============== whitespace mode ======================
(setq whitespace-style (quote (spaces tabs newline space-mark tab-mark newline-mark)))
(setq whitespace-display-mappings
      ;; all numbers are Unicode codepoint in decimal. e.g. (insert-char 182 1)
      '(
        (space-mark 32 [183] [46]) ; 32 SPACE 「 」, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
        (newline-mark 10 [182 10]) ; 10 LINE FEED
        (tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
        ))


;; ===================== Undo-tree ====================
(use-package undo-tree
  :defer t
  :ensure t
  :diminish undo-tree-mode
  :init
  (progn
    (global-undo-tree-mode)
    (setq undo-tree-visualizer-timestamps t)
    (setq undo-tree-visualizer-diff t)))



; ================ multiple cursors ===============
(use-package multiple-cursors
  :ensure t 
  :defer t
  :bind
   (("C-c m t" . mc/mark-all-like-this)
    ("C-c m m" . mc/mark-all-like-this-dwim)
    ("C-c m l" . mc/edit-lines)
    ("C-c m e" . mc/edit-ends-of-lines)
    ("C-c m a" . mc/edit-beginnings-of-lines)
    ("C-c m n" . mc/mark-next-like-this)
    ("C-c m p" . mc/mark-previous-like-this)
    ("C-c m s" . mc/mark-sgml-tag-pair)
    ("C-c m d" . mc/mark-all-like-this-in-defun)))

; (use-package mc-extras
;   :ensure t 
;   :defer t
;   :config
;     (define-key mc/keymap (kbd "C-. =") 'mc/compare-chars))


;; =================== ORG-mode ====================
(load-file (concat user-emacs-directory "myorg.el"))


;; =================== desktop etc. ====================
(load-file (concat user-emacs-directory "mydesktop.el"))



;; ===================== IDO ====================
(load-file (concat user-emacs-directory "myido.el"))

(defun ido-choose-from-recentf ()
  "Use `ido-completing-read' to find a recent file."
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))



;; =================== duplicate-thing =================
; (use-package duplicate-thing
;   :ensure t
;   :bind      ("C-c d" . duplicate-thing))



;; ===================== ediff ==========================
(setq ediff-split-window-function 'split-window-horizontally)



;; ==================== volatile-highlights ============
; (use-package volatile-highlights
;   :ensure t
;   :config (volatile-highlights-mode t)
;   :diminish volatile-highlights-mode)


;; ================ dired ====================
; (use-package dired+
;   :ensure t
;   :commands toggle-diredp-find-file-reuse-dir)

; (use-package dired
;   :ensure t
;   :commands (dired
;              find-name-dired
;              find-dired)
;   :config
;     (toggle-diredp-find-file-reuse-dir 1))




;; =============== expand-region ==============
; (use-package expand-region
;   :ensure t
;   :bind ("C-=" . er/expand-region))

    
;; =============== ace-jump-mode ===============
; (use-package ace-jump-mode
;   :ensure t
;   :commands ace-jump-mode
;   :init
;   (bind-key "C-." 'ace-jump-mode))




;; =================== nxml ========================
(defun my-nxml-hook ()
  (setq c-tab-always-indent t
        tab-width 4
        indent-tabs-mode t)) 
(add-hook 'nxml-mode-hook 'my-nxml-hook)


;; ================= My functions ===================
(load-file (concat user-emacs-directory "myfunctions.el"))



;; ================= KEY remap ===============
(load-file (concat user-emacs-directory "mykeys.el"))



; ; ================ markdown mode =====================
; (use-package markdown-mode
;   :ensure t
;   :mode ("\\.\\(m\\(ark\\)?down\\|md\\)$" . markdown-mode)
;     ; :config
;     ; (bind-key "<backspace>" #'cua-delete-region markdown-mode-map)
; )



(provide 'myinit)
