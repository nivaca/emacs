(electric-pair-mode 1)

(setq-default electric-pair-inhibit-predicate
              (lambda (c)
                (if (looking-at "[ \n\t]")
                    (electric-pair-default-inhibit c)
                    t)))

(add-hook 'LaTeX-mode-hook
          '(lambda ()
            (define-key LaTeX-mode-map (kbd "$") 'self-insert-command)))






; (use-package smartparens-config
;   :ensure smartparens
;   :diminish smartparens-mode
;   :config
;   (progn
;     (smartparens-global-mode 1)
;     (show-smartparens-global-mode 1)
;     (setq
;        smartparens-strict-mode 1
;        sp-autoinsert-pair 1
;        sp-autoskip-closing-pair 'always
;        sp-base-key-bindings 'paredit
;        sp-hybrid-kill-entire-symbol nil
;        )
;     ))

; ;;; tex-mode latex-mode
; (sp-with-modes '(tex-mode plain-tex-mode latex-mode)
;   (sp-local-tag "i" "\"<" "\">")
;   )

; ;; LaTeX modes
; (sp-with-modes '(
;                  tex-mode
;                  plain-tex-mode
;                  latex-mode
;                  )
;   ;; math modes, yay. The :actions are provided automatically if
;   ;; these pairs do not have global definition.
;   (sp-local-pair "$" "$")
;   (sp-local-pair "\[" "\]")
;   (sp-local-pair "\{" "\}")
;   (sp-local-pair "‘" "’")
;   (sp-local-pair "“" "”")
;   (sp-local-tag "b" "\begin{}" "\end{}")
;   )


; ;;; markdown-mode
; (sp-with-modes '(markdown-mode)
;   (sp-local-pair "*" "*" :bind "C-*")
;   (sp-local-tag "2" "**" "**")
;   (sp-local-tag "s" "```scheme" "```")
;   (sp-local-tag "<" "<_>" "</_>" :transform 'sp-match-sgml-tags))




; ;; Text modes
; (sp-with-modes '(text-mode)
;   ;; math modes, yay. The :actions are provided automatically if
;   ;; these pairs do not have global definition.
;   (sp-local-pair "\[" "\]")
;   (sp-local-pair "\{" "\}")
;   (sp-local-pair "‘" "’")
;   (sp-local-pair "“" "”")
;   )

; (setq sp-ignore-modes-list
;   (delete 'minibuffer-inactive-mode sp-ignore-modes-list))

(provide 'myparent)
