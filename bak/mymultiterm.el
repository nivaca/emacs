(use-package multi-term
  :ensure t
  :bind ("<f11>" . multi-term)
  :defer t
  :config
  (progn
    (setq multi-term-dedicated-select-after-open-p t
          multi-term-dedicated-window-height 25
          multi-term-program "/usr/local/bin/zsh"
          system-uses-terminfo nil
          )
  )
)
