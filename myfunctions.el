(defun nv/unfill-paragraph ()
 "Takes a multi-line paragraph and makes it into a single line of text."
 (interactive)
 (let ((fill-column (point-max)))
   (fill-paragraph nil)))


(defun nv/remove-newlines-in-region ()
  "Removes all newlines in the region."
  (interactive)
  (save-restriction
    (narrow-to-region (point) (mark))
    (goto-char (point-min))
    (while (search-forward "\n" nil t) (replace-match " " nil t))))


;; =========== kill all buffers =============
;; https://stackoverflow.com/questions/3417438/closing-all-other-buffers-in-emacs
(defun nv/kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))



;; ============= Commenting ===============
(defun nv/comment-or-uncomment-line-or-region ()
  "Comments or uncomments the current line or region."
  (interactive)
  (if (region-active-p)
      (comment-or-uncomment-region (region-beginning) (region-end))
    (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    )
  )



;; ======================================
;; smart delete backline (oXygen)
(defun nv/delete_back (&optional param)
  "Deletes empty lines."
  (interactive)
  (if (not param)
      ;; then
      (progn
        ;; define a "nor (not or)" operator
        (defun nor (a b) (not (or a b)))
        ;;
        (if (nor (looking-back "[\s-]")
                 (looking-back "[\n]")
                 )
            ;;then
            (delete-backward-char 1)
          ;;else
          (while
              (looking-back "[\s-]")
            (delete-backward-char 1)
            )
          )
        )
    ;; else
    (progn
      (defun nor (a b) (not (or a b)));; not or
      (if (nor (looking-back "[\s-]")
               (looking-back "[\n]")
               )
          ;;then
          (delete-backward-char 1)
        ;;else
        (while
            (or (looking-back "[\s-]")
                (looking-back "[\n]")
                )
          (delete-backward-char 1)
          )
        )
      )
    )
  )








;; -------------------
(provide 'myfunctions)
