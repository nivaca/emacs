;; My init file

(eval-when-compile (require 'cl)) ;; Require Common Lisp (for "case" etc.)


(case system-type
  ('gnu/linux
    (progn
      (when (string= system-name "nivaca-HP")
        (setq user-emacs-directory "~/Dropbox/emacs_x/"))
      (when (string= system-name "nivaca-DELL")
        (setq user-emacs-directory "~/Dropbox/emacs_d/"))
    )
  )
  ('darwin
    (progn
     (setq user-emacs-directory "~/Dropbox/emacs/")
     (setq delete-by-moving-to-trash t)
      (defun system-move-file-to-trash (file)
        "Use \"trash\" to move FILE to the system trash.
      When using Homebrew, install it using \"brew install trash\"."
        (call-process (executable-find "trash")
          nil 0 nil
          file))
    )
  )
)



(let ((default-directory user-emacs-directory))
  (normal-top-level-add-subdirs-to-load-path)
  )


(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setq exec-path (append exec-path '("/usr/local/bin")))




; ------------------------------------------------------------
;;; Turn off the annoying crap immediately
(setq backup-inhibited t
      auto-save-default t
      inhibit-startup-message t
      dabbrev-case-distinction nil
      dabbrev-case-fold-search nil
      vc-follow-symlinks t ;; Follow symbolic links
      echo-keystrokes 0.1
      disabled-command-function nil
      large-file-warning-threshold 536870911)



;; Remove text in active region if inserting text
(delete-selection-mode 1)

;; Don't highlight matches with jump-char - it's distracting
(setq jump-char-lazy-highlight-face nil)

;; Never insert tabs
(set-default 'indent-tabs-mode nil)

;; Easily navigate sillycased words
(global-subword-mode 1)

;; Don't break lines for me, please
(setq-default truncate-lines t)

;; Allow recursive minibuffers
(setq enable-recursive-minibuffers t)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq message-log-max t)

;; Share clipboard
(setq x-select-enable-clipboard t)




;; ==================== UTF-8 =======================
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq buffer-file-coding-system 'utf-8)
;; From Emacs wiki
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))



;; ================= Package Management ===============
(load-file (expand-file-name "mypackages.el" user-emacs-directory))


; ------------------------------------------------------------
;;;; Modes and mode groupings
(defmacro hook-into-modes (func modes)
  "Add hook `FUNC' to multiple `MODES'."
  `(dolist (mode-hook ,modes)
  (add-hook mode-hook ,func)))


;; ==================== Server ====================
(if (not server-mode)
    (server-start nil t))


;; ================ My display configuration =============
(load-file (expand-file-name "mydisplay.el" user-emacs-directory))


;; ===================== AUCTEX =========================
(load-file (expand-file-name "myauctex.el" user-emacs-directory))


;; ========== Disable native GIT support ==============
(setq vc-handled-backends nil)
(setq vc-handled-backends ())




;; ================= FLYSPELL ============================
(use-package flyspell
  :ensure t
  :config
  (progn
    (define-key flyspell-mode-map [down-mouse-3] 'flyspell-correct-word)
    (if (eq system-type 'darwin)
        (setq ispell-program-name "aspell"
              aspell-dictionary "en_GB-ise-wo_accents"
              aspell-program-name "/usr/local/bin/aspell"
              ispell-dictionary "en_GB-ise-wo_accents"
              ispell-program-name "/usr/local/bin/aspell")
      )
    (if (eq system-type 'gnu/linux)
      (progn
        (setq ispell-program-name "aspell"
              aspell-dictionary "en_GB-ise-wo_accents"
              aspell-program-name "/usr/bin/aspell"
              ispell-dictionary "en_GB-ise-wo_accents"
              ispell-program-name "/usr/bin/aspell")
                    (unbind-key "C-." flyspell-mode-map)
                    (unbind-key "C-," flyspell-mode-map)
                   (bind-key "C-." 'comment-or-uncomment-line-or-region)
                   (bind-key "C-," 'comment-or-uncomment-line-or-region)
        )
      )
    (defun turn-on-flyspell ()
      "Force flyspell-mode on using a positive arg.  For use in hooks."
      (interactive)
      (flyspell-mode 1))
    (add-hook 'LaTeX-mode-hook 'flyspell-mode)
    (add-hook 'message-mode-hook 'turn-on-flyspell)
    ;; (add-hook 'text-mode-hook 'turn-on-flyspell)
    )
  )


;; ===============  Yasnippet  ===============
(use-package yasnippet
  :if (not noninteractive)
  :diminish yas-minor-mode
  :commands (yas-global-mode yas-minor-mode)
  :ensure t
  :config
  (progn
    (setq
      yas-indent-line nil
      yas-snippet-dirs (list (expand-file-name "snippets" user-emacs-directory))
      )
    )
  )


;; ============= Commenting ===============
(load-file (expand-file-name "mycomments.el" user-emacs-directory))



;; =================  parentheses ================
(load-file (expand-file-name "myparent.el" user-emacs-directory))


;; =============== whitespace mode ======================
(setq whitespace-style (quote (spaces tabs newline space-mark tab-mark newline-mark)))
(setq whitespace-display-mappings
      ;; all numbers are Unicode codepoint in decimal. e.g. (insert-char 182 1)
      '(
        (space-mark 32 [183] [46]) ; 32 SPACE 「 」, 183 MIDDLE DOT 「·」, 46 FULL STOP 「.」
        (newline-mark 10 [182 10]) ; 10 LINE FEED
        (tab-mark 9 [9655 9] [92 9]) ; 9 TAB, 9655 WHITE RIGHT-POINTING TRIANGLE 「▷」
        ))


;; ===================== Undo-tree ====================
(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config
  (progn
    (global-undo-tree-mode)
    (setq undo-tree-visualizer-timestamps t)
    (setq undo-tree-visualizer-diff t)
  )
)


; ===================== move-text ====================
(use-package drag-stuff
  :ensure t
)


; ===================== crux ====================
(use-package crux
  :ensure t
  :bind
  (
    ;; ("C-<backspace>" . crux-kill-line-backwards)
    ("C-a" . crux-move-beginning-of-line)
    ("C-e" . move-end-of-line)
    ("<home>" . crux-move-beginning-of-line)
    ("<end>" . move-end-of-line)
    ("C-c d" . crux-duplicate-current-line-or-region)
    )
)



; ================ multiple cursors ===============
(load-file (expand-file-name "mymc.el" user-emacs-directory))



;; =================== ORG-mode ====================
(load-file (expand-file-name "myorg.el" user-emacs-directory))


;; =================== desktop etc. ====================
(load-file (expand-file-name "mydesktop.el" user-emacs-directory))


;; =================== bookmarks ====================
(load-file (expand-file-name "mybm.el" user-emacs-directory))


;; ===================== ediff ==========================
(setq ediff-split-window-function 'split-window-horizontally)



;; ================= company ==================
(load-file (expand-file-name "mycompany.el" user-emacs-directory))



;; =============== expand-region ==============
(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))


;; ============== swiper - ivy ==============
(load-file (expand-file-name "myivy.el" user-emacs-directory))


;; =================== nxml ===================
(defun my-nxml-hook ()
  (setq c-tab-always-indent t
        tab-width 4
        indent-tabs-mode t))
(add-hook 'nxml-mode-hook 'my-nxml-hook)


;; ===================================================
(defun ensure-region-active (func &rest args)
  (when (region-active-p)
    (apply func args)))

(advice-add 'upcase-region :around 'ensure-region-active)
(advice-add 'downcase-region :around 'ensure-region-active)


;; ================= My functions ===================
(load-file (expand-file-name "myfunctions.el" user-emacs-directory))


;; ================= My aliases ===================
(load-file (expand-file-name "myaliases.el" user-emacs-directory))


;; ================= KEY remap ===============
(load-file (expand-file-name "mykeys.el" user-emacs-directory))




(provide 'myinit)
