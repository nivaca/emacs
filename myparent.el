(use-package smartparens-config
  :ensure smartparens
  :diminish smartparens-mode
  :config
  (progn
    (smartparens-global-mode 1)
    (show-smartparens-global-mode 1)
    (setq
       smartparens-strict-mode 1
       sp-autoinsert-pair 1
       sp-autoskip-closing-pair 'always
       sp-base-key-bindings 'paredit
       sp-hybrid-kill-entire-symbol nil
       )
    ))


(sp-pair "(" nil :unless '(sp-point-before-word-p))
(sp-pair "[" nil :unless '(sp-point-before-word-p))
(sp-pair "{" nil :unless '(sp-point-before-word-p))


;; LaTeX modes
(sp-with-modes '(
                 tex-mode
                 plain-tex-mode
                 latex-mode
                 )
  ;; math modes, yay. The :actions are provided automatically if
  ; these pairs do not have global definition.
  (sp-local-pair "$" "$")
  (sp-local-pair "\[" "\]")
  (sp-local-pair "\{" "\}")
  (sp-local-pair "‘" "’")
  (sp-local-pair "“" "”")
  (sp-local-pair "\\begin" "\\end")
  ;;; tex-mode latex-mode
  (sp-local-tag "i" "\"<" "\">")
  (sp-local-pair "\\[" nil :unless '(sp-point-before-word-p))
  (sp-local-pair "$" nil :unless '(sp-point-before-word-p))
  )





;;; markdown-mode
(sp-with-modes '(markdown-mode)
  (sp-local-pair "*" "*" :bind "C-*")
  (sp-local-tag "2" "**" "**")
  (sp-local-tag "s" "```scheme" "```")
  (sp-local-tag "<" "<_>" "</_>" :transform 'sp-match-sgml-tags))




;; Text modes
(sp-with-modes '(text-mode)
  ;; math modes, yay. The :actions are provided automatically if
  ;; these pairs do not have global definition.
  (sp-local-pair "\[" "\]")
  (sp-local-pair "\{" "\}")
  (sp-local-pair "‘" "’")
  (sp-local-pair "“" "”")
  )

(setq sp-ignore-modes-list
  (delete 'minibuffer-inactive-mode sp-ignore-modes-list))

(provide 'myparent)
