;; Remap keys

(global-set-key (quote [f1]) 'delete-other-windows)
(global-set-key (quote [C-f1]) 'clone-indirect-buffer)

(global-set-key (quote [f2]) 'save-buffer)
;; F3: Begin define kmacro
;; F4: End define macro
(global-set-key (quote [f5]) 'org-mode)
(global-set-key (quote [f6]) 'lisp-mode)
(global-set-key (quote [f7]) 'latex-mode)
(global-set-key [(f9)] 'recentf-open-files)

(global-unset-key (quote [f8]))
(global-unset-key (quote [f10]))

; (global-set-key (quote [f11])  'eshell)


(global-set-key (quote [f12]) 'execute-extended-command)
(global-set-key (quote [S-f12]) 'eval-expression)

;; (global-unset-key (quote [f12]))



(bind-key* "\C-x\C-m" 'execute-extended-command)

(global-set-key (kbd "C-S-z") 'undo-tree-redo)
(global-set-key (kbd "C-/") 0)

(global-set-key (kbd "C-x C-b") 'ibuffer)


(case system-type
  ('darwin
    ;; (define-key global-map (kbd "M-/") (lambda () (interactive) (insert "¿")))
   ;; (define-key global-map (kbd "M-n") (lambda () (interactive) (insert "ñ")))
   (setq mac-option-key-is-meta nil)
    (setq mac-command-key-is-meta t)
    (setq mac-command-modifier 'meta)
    (setq mac-option-modifier nil)
    )
)


;; ----------------------------------------------
;; Deactivate dangerous keys
;; (global-unset-key [(control w)])
(global-unset-key "\C-x\C-z")
(global-unset-key "\M-y")
(global-unset-key "\C-w")
(global-unset-key "\M-w")
(global-set-key (kbd "\C-w") 'kill-ring-save)  ;; copy


;; Don't ask which buffer to kill
;; (global-set-key (kbd "C-w")
(global-set-key (kbd "C-x k")
  '(lambda () (interactive)
     (let (kill-buffer-query-functions) (kill-buffer))))



;; -------------------------------------------
;; nv/delete_back (myfunctions.el)
(bind-key "C-<backspace>" 'nv/delete_back)
(bind-key "S-C-<backspace>"
          '(lambda () (interactive) (nv/delete_back 1))
          )
;; (bind-key "M-<backspace>" 'nv/backward-kill-word)




; Map escape to cancel (like C-g)...
(define-key isearch-mode-map [escape] 'isearch-abort)   ;; isearch
(define-key isearch-mode-map "\e" 'isearch-abort)   ;; \e seems to work better for terminals
(global-set-key [escape] 'keyboard-escape-quit)         ;; everywhere else


(global-unset-key (kbd "<C-down-mouse-1>"))  ;; disable "buffer menu"
(global-unset-key (kbd "<C-down-mouse-2>"))  ;; disable "buffer menu"
(global-unset-key (kbd "<C-down-mouse-3>"))  ;; disable "buffer menu"

;; extend region with mouse
;; https://superuser.com/questions/521223/shift-click-to-extend-marked-region
(define-key global-map (kbd "<S-down-mouse-1>") 'mouse-save-then-kill)




(case system-type
  ('gnu/linux
   (progn
     ;; Comment line
     (bind-key* "C--" 'nv/comment-or-uncomment-line-or-region) ;; defined in myfunctions.el
     ;;  AucTeX autocomplete
     ; (add-hook 'LaTeX-mode-hook (lambda()
                                  ; (local-set-key [C-tab] 'TeX-complete-symbol)))
     )
   )
  ('darwin
   (progn
     ;; Comment line
     (bind-key* "C-/" 'nv/comment-or-uncomment-line-or-region) ;; defined in myfunctions.el
     ;;  AucTeX autocomplete
     ; (add-hook 'LaTeX-mode-hook (lambda() (local-set-key [M-tab] 'TeX-complete-symbol)))
     (setq
      mac-command-modifier 'control
      mac-control-modifier 'meta
      mac-pass-command-to-system nil)
     )
   )
)





; ================ undo  ==================
;; Mac style undo
(global-set-key (kbd "C-z") 'undo-tree-undo) ; 【Ctrl+z】
;; Mac style redo
(global-set-key (kbd "M-z") 'undo-tree-redo) ; 【Alt+z】


(bind-key* "C-k" 'kill-region)  ; Cut
(global-unset-key (kbd "C-v"))

; (global-set-key "\C-x{" 'backward-page)
; (global-set-key "\C-x}" 'forward-page)

; ----------------- Zoom --------------
; Requires use-package
; (bind-key "C-=" 'text-scale-increase)
; (bind-key "C--" 'text-scale-decrease)



;; http://emacs.stackexchange.com/a/11064/516
(defun my-keyboard-quit-advice (fn &rest args)
  (let ((region-was-active (region-active-p)))
    (unwind-protect
         (apply fn args)
      (when region-was-active
        (activate-mark t)))))

(advice-add 'keyboard-quit :around #'my-keyboard-quit-advice)


;; disable SPC autocomplete in minibuffer
(define-key minibuffer-local-completion-map (kbd "SPC") 'self-insert-command)


;; Abolish secondary selection
(global-set-key [remap mouse-drag-secondary] 'mouse-drag-region)
(global-set-key [remap mouse-set-secondary] 'mouse-set-region)
(global-set-key [remap mouse-start-secondary] 'mouse-set-point)
(global-set-key [remap mouse-yank-secondary] 'mouse-yank-primary)
(global-set-key [remap mouse-secondary-save-then-kill] 'mouse-save-then-kill)

(provide 'mykeys)
