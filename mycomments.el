;; ============= Commenting ===============
(defun comment-or-uncomment-line-or-region ()
  "Comments or uncomments the current line or region."
  (interactive)
  (if (region-active-p)
      (comment-or-uncomment-region (region-beginning) (region-end))
    (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    )
  )


;; ================ comment lines ===================
(case system-type
  ('gnu/linux
   (progn
     ;; Comment line
     (bind-key* "C--" 'comment-or-uncomment-line-or-region)
     ;;  AucTeX autocomplete
     (add-hook 'LaTeX-mode-hook (lambda()
                                  (local-set-key [C-tab] 'TeX-complete-symbol)))
     )
   )
  ('darwin
   (progn
     ;; Comment line
     (bind-key* "C-/" 'comment-or-uncomment-line-or-region)
     ;;  AucTeX autocomplete
     (add-hook 'LaTeX-mode-hook (lambda() (local-set-key [M-tab] 'TeX-complete-symbol)))
     (setq
      mac-command-modifier 'control
      mac-control-modifier 'meta
      mac-pass-command-to-system nil)
     )
   )
)



(provide 'mycomments)