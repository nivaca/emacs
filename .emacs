
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(load "~/Dropbox/emacs/myinit.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" default)))
 '(ispell-dictionary "castellano")
 '(ispell-program-name "/usr/local/bin/aspell")
 '(package-selected-packages
   (quote
    (highlight-indent-guides auto-indent-mode drag-stuff mwim counsel-osx-app counsel sublimity zenburn-theme bm crux folding nlinum paradox use-package undo-tree swiper smooth-scrolling smartparens scroll-restore scratch-pop pretty-mode multiple-cursors move-text julia-mode jedi elpy company-auctex color-theme async adaptive-wrap)))
 '(safe-local-variable-values
   (quote
    ((eval ispell-change-dictionary "english")
     (adaptative-wrap-prefix-mode . 1)
     (eval ispell-change-dictionary "en-GB_ise")
     (eval ispell-change-dictionary "castellano")
     (eval ispell-change-dictionary "espanol")
     (eval ispell-change-dictionary "spanish")))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
