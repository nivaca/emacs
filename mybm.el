(use-package bm
  :ensure t
  :demand t

  :init
  (setq bm-restore-repository-on-load t)
  :config
  (setq bm-cycle-all-buffers t)
  (setq bm-repository-file (concat user-emacs-directory "tmp/bm-repository"))
  (setq-default bm-buffer-persistence t)
  (add-hook' after-init-hook 'bm-repository-load)
  (add-hook 'find-file-hooks 'bm-buffer-restore)
  (add-hook 'kill-buffer-hook #'bm-buffer-save)
  (defun modi/bm-save-all-bm-to-repository ()
    (bm-buffer-save-all)
    (bm-repository-save)
    )
  (add-hook 'kill-emacs-hook #'modi/bm-save-all-bm-to-repository)
  (add-hook 'after-save-hook #'bm-buffer-save)
  (setq bm-highlight-style 'bm-highlight-only-fringe)
  ;; Restoring bookmarks
  (add-hook 'find-file-hooks   #'bm-buffer-restore)
  (add-hook 'after-revert-hook #'bm-buffer-restore)
  (add-hook 'vc-before-checkin-hook #'bm-buffer-save)
  :bind (
         ;; ("<left-fringe> <wheel-up>" . bm-next-mouse)
         ;; ("<left-margin> <wheel-up>" . bm-next-mouse)
         ;; ("<left-fringe> <wheel-down>" . bm-previous-mouse)
         ;; ("<left-margin> <wheel-down>" . bm-previous-mouse)
         ("<C-f8>" . bm-toggle)
	 ("<f8>" . bm-next)
         ("<S-f8>" . bm-previous)
         ("<left-fringe> <mouse-1>" . bm-toggle-mouse)
         ("<left-margin> <mouse-1>" . bm-toggle-mouse))
  )

(provide 'mybm)
