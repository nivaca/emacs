(require 'package)


(setq package-enable-at-startup nil)


(setq
 package-user-dir (concat user-emacs-directory "elpa")
 package-archives
 '(("melpa" . "http://melpa.org/packages/")
   ("marmalade" . "https://marmalade-repo.org/packages/")
   ("org" . "http://orgmode.org/elpa/")
   ("gnu" . "http://elpa.gnu.org/packages/")
   )
 package-archive-exclude-alist
 '(("melpa" elnode)
   ("melpa" org-trello)
   ("melpa" org)
   ("marmalade" org)
   ("gnu" org)
   ("org" org))
 )


(package-initialize)


(require 'use-package)


(use-package async
  :ensure t)


; ------------------ paradox -----------------------
(use-package paradox
  :ensure t
  ; :bind (("C-x C-u" . paradox-upgrade-packages))
  :config
    (setq paradox-execute-asynchronously t
        paradox-spinner-type 'vertical-breathing
        paradox-display-download-count t
        paradox-display-star-count t
        paradox-github-token "633f1c14facc966e78ba4d1846bada05cb7ebf9e")
  :commands
  (paradox-upgrade-packages paradox-list-packages)
)


(provide 'mypackages)