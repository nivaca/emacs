;; ============== swiper - ivy ==============
(use-package swiper
  :ensure t
  :ensure ivy
  :ensure counsel
  :diminish ivy-mode
  :bind (("\C-s" . swiper)
    ("\C-r" . swiper)
    ("C-c C-r" . ivy-resume)
    ("<f6>" . ivy-resume)
    ("M-x" . counsel-M-x)
    ; ("<f12>" . counsel-M-x)
    ("C-x C-f" . counsel-find-file)
    ("C-h f" . counsel-describe-function)
    ("C-h v" . counsel-describe-variable)
    ; ("C-h l" . counsel-load-library)
    ; ("C-h i" . counsel-info-lookup-symbol)
    ; ("C-h u" . counsel-unicode-char)
    ; ("C-c g" . counsel-git)
    ; ("C-c j" . counsel-git-grep)
    ; ("C-c k" . counsel-ag)
    ; ("C-x l" . counsel-locate)
    ; ("C-S-o" . counsel-rhythmbox)
    )
  :init
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t
      ivy-display-style 'fancy
      ivy-height 10
      ivy-count-format ""
      ivy-initial-inputs-alist nil
      ivy-re-builders-alist
    ;; allow input not in order
          '((t   . ivy--regex-ignore-order))
    )
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    (defun bjm-swiper-recenter (&rest args)
      "recenter display after swiper"
      (recenter)
      )
    (advice-add 'swiper :after #'bjm-swiper-recenter)
    (bind-keys :map ivy-minibuffer-map
          ("C-<down>" . ivy-next-history-element)
          ("C-<up>" . ivy-previous-history-element))

))

(provide 'myivy)