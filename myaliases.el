;; Some aliases
(defalias 'er 'eval-region)
(defalias 'eb 'eval-buffer)
(defalias 'rs 'replace-string)
(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'p-u-p 'paradox-upgrade-packages)


(provide 'myaliases)