; mydesktop.el
; Desktop, savaplace, recentf

;; ================ desktop save ===============
(desktop-save-mode 1)

(setq desktop-dirname             (concat user-emacs-directory "tmp/")
      desktop-base-file-name      "emacs-desktop"
      desktop-base-lock-name      "lock"
      desktop-path                (list desktop-dirname)
      desktop-save                t
      ; desktop-files-not-to-save   "^$" ;reload tramp paths
      desktop-load-locked-desktop nil)

(add-to-list 'desktop-globals-to-save 'file-name-history)
(add-to-list 'desktop-modes-not-to-save 'dired-mode)
(add-to-list 'desktop-modes-not-to-save 'Info-mode)
(add-to-list 'desktop-modes-not-to-save 'info-lookup-mode)
(add-to-list 'desktop-modes-not-to-save 'fundamental-mode)


;; Remember mini-buffer history
(savehist-mode 1)
(setq
  savehist-additional-variables '(kill-ring search-ring regexp-search-ring)
  savehist-file (concat user-emacs-directory "tmp/savehist")
)


;; =================== saveplace ===================
; save the place in files
(use-package saveplace
  :ensure t
  :config
  (setq-default save-place t)
  (setq save-place-forget-unreadable-files nil)
  (setq save-place-file (concat user-emacs-directory "tmp/saved-places")))



;; ============= recentf stuff ================
(use-package recentf
  :ensure t
  :config
  (progn
    (setq recentf-max-saved-items 300
          recentf-save-file (concat user-emacs-directory "tmp/recentf")
          recentf-exclude '("/auto-install/" ".recentf" "/repos/" "/elpa/"
                            "\\.mime-example" "\\.ido.last" "COMMIT_EDITMSG"
                            ".gz"
                            "~$" "/tmp/" "/ssh:" "/sudo:" "/scp:")
          recentf-auto-cleanup 600)
    (when (not noninteractive) (recentf-mode 1))

    (defun recentf-save-list ()
      "Save the recent list.
Load the list from the file specified by `recentf-save-file',
merge the changes of your current session, and save it back to
the file."
      (interactive)
      (let ((instance-list (copy-list recentf-list)))
        (recentf-load-list)
        (recentf-merge-with-default-list instance-list)
        (recentf-write-list-to-file)))

    (defun recentf-merge-with-default-list (other-list)
      "Add all items from `other-list' to `recentf-list'."
      (dolist (oitem other-list)
        ;; add-to-list already checks for equal'ity
        (add-to-list 'recentf-list oitem)))

    (defun recentf-write-list-to-file ()
      "Write the recent files list to file.
Uses `recentf-list' as the list and `recentf-save-file' as the
file to write to."
      (condition-case error
          (with-temp-buffer
            (erase-buffer)
            (set-buffer-file-coding-system recentf-save-file-coding-system)
            (insert (format recentf-save-file-header (current-time-string)))
            (recentf-dump-variable 'recentf-list recentf-max-saved-items)
            (recentf-dump-variable 'recentf-filter-changer-current)
            (insert "\n \n;;; Local Variables:\n"
                    (format ";;; coding: %s\n" recentf-save-file-coding-system)
                    ";;; End:\n")
            (write-file (expand-file-name recentf-save-file))
            (when recentf-save-file-modes
              (set-file-modes recentf-save-file recentf-save-file-modes))
            nil)
        (error
         (warn "recentf mode: %s" (error-message-string error)))))))

(recentf-mode t)



;; minibuffer history-length
(savehist-mode 1)
(setq savehist-file (concat user-emacs-directory "tmp/savehist"))
(setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))



;; Disable autosave
(setq auto-save-default nil) 
(setq make-backup-files t)    ; don't make backup files


(provide 'mydesktop)