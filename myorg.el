;; Org-mode settings


(use-package org
  :ensure t)

(setq org-replace-disputed-keys nil
  org-support-shift-select t
  )


(eval-after-load 'org
  (progn
    (define-key org-mode-map (kbd "<S-right>") nil)
    (define-key org-mode-map (kbd "<S-left>") nil)))



(provide 'myorg)