;;; Compiled snippets and support files for `latex-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'latex-mode
                     '(("it" "\\textit{$0}" "textit" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/textit" nil nil)
                       ("nin" "\\noindent\n" "noindent" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/noindent" nil nil)
                       ("fn" "%\n  %\n  \\footnote{$0\n  }\n  %\n\n" "footnote" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/footnote" nil nil)
                       ("fg" "\n\\begin{figure}[!ht]\n  \\centering\n  \\includegraphics[width=\\linewidth]{}\n  \\caption{}\n  \\label{fig:}\n\\end{figure} " "\\begin{figure} ... \\end{figure}" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/figure" nil nil)
                       ("cita" "\\begin{cita}\n  $0\n\\end{cita}" "\\begin{cita} ... \\end{cita}" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/cita" nil nil)
                       ("begin" "\n\\begin{${1:environment}}\n$0\n\\end{$1}" "\\begin{environment} ... \\end{environment}" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/latex-mode/begin" nil nil)))


;;; Do not edit! File generated at Sun Feb 14 15:09:34 2016
