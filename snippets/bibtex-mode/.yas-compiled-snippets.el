;;; Compiled snippets and support files for `bibtex-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'bibtex-mode
                     '(("@th" "@thesis{,\n  Title = {},\n  Author = {},\n  Type = {},\n  Institution = {},\n  Year = {},\n}\n" "thesis" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/bibtex-mode/thesis" nil nil)
                       ("@in" "@incollection{,\n  Title = {},\n  Author = {},\n  Booktitle = {},\n  Editor = {},\n  Publisher = {},\n  Location = {},\n  Pages = {},\n  Year = {},\n}\n" "incollection" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/bibtex-mode/incollection" nil nil)
                       ("@com" "@comment{$0}" "comment" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/bibtex-mode/comment" nil nil)
                       ("@bo" "@book{,\n  Title = {},\n  Author = {},\n  Publisher = {},\n  Location = {},\n  Year = {},\n}\n" "book" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/bibtex-mode/book" nil nil)
                       ("@ar" "@article{,\n  Title = {},\n  Author = {},\n  Journal = {},\n  Volume = {},\n  Number = {},\n  Pages = {},\n  Year = {},\n}\n" "article" nil nil nil "/Volumes/ElDuro/nivaca/Dropbox/emacs/snippets/bibtex-mode/article" nil nil)))


;;; Do not edit! File generated at Sun Feb 14 15:09:34 2016
