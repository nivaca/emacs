;; My company mode config

(use-package company
  :ensure
  :init (progn
          (global-company-mode)
          (setq company-global-modes '(not python-mode cython-mode sage-mode))
          )
  :config (progn
            (setq company-tooltip-limit 20
                  company-begin-commands '(self-insert-command)
                  company-transformers '(company-sort-by-occurrence)
                  company-selection-wrap-around t
                  company-idle-delay 60
                  company-minimum-prefix-length 1
                  company-selection-wrap-around t
                  ;; company-dabbrev-downcase nil
                  )
            (delete 'company-dabbrev company-backends)
            (delete '(company-dabbrev-code company-gtags company-etags company-keywords) company-backends)
            ;;
            (use-package company-auctex
              :ensure t
              :config
              (progn
                (company-auctex-init)
                (add-to-list 'company-backends '(company-auctex))
                )
              )
            ;;
            (case system-type
              ;; ----------------------------------------
              ('gnu/linux ;; LINUX
               (bind-keys
                ("<s-tab>" . company-complete-common)
                :map company-active-map
                ("C-n" . company-select-next)
                ("C-p" . company-select-previous)
                ("C-d" . company-show-doc-buffer)
                ;; ("<tab>" . company-complete)
                ("<escape>" . company-abort)
                )
               )
              ;; ---------------------------------------
              ('darwin ;; MAC OS X
               (bind-keys
                ("<M-tab>" . company-complete-common)
                :map company-active-map
                ("C-n" . company-select-next)
                ("C-p" . company-select-previous)
                ("C-d" . company-show-doc-buffer)
                ;; ("<tab>" . company-complete)
                ("<escape>" . company-abort)
                )
               )
              )
            )
  )




(provide 'mycompany)
