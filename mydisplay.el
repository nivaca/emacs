;; mydisplay.el

(setq visible-bell t
      font-lock-maximum-decoration t
      color-theme-is-global t
      truncate-partial-width-windows nil)


;; Show me empty lines after buffer end
(set-default 'indicate-empty-lines t)


;; Small fringes
                                        ; (set-fringe-mode '(1 . 1))


;; Don't defer screen updates when performing operations
(setq redisplay-dont-pause t)


;; Highlight matching parentheses when the point is on them.
(show-paren-mode 1)

(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode -1)
  (blink-cursor-mode -1))


;; ---------------------- window --------------------------

;; Set Frame width/height
(defun arrange-frame (w h x y)
  "Set the width, height, and x/y position of the current frame"
  (let ((frame (selected-frame)))
    (delete-other-windows)
    (set-frame-position frame x y)
    (set-frame-size frame w h)))

;; Force opening files in existing frames
(setq ns-pop-up-frames nil)

(case system-type
  ;; -------------------------------------------------------
  ('gnu/linux ;; LINUX
   (progn
      ; (when (string= system-name "nivaca-HP")
      ;   (arrange-frame 71 38 1 1))
      ; (when (string= system-name "nivaca-DELL")
      ;   (arrange-frame 71 36 1 1))
     ;; Mouse wheel
     (setq scroll-step 1
           mouse-wheel-progressive-speed 1
           mouse-wheel-scroll-amount '(1 ((shift) . 1))
           mouse-wheel-follow-mouse 't)
     (set-face-attribute 'default nil :font "DejaVu Sans Mono-11")
     (setq color-theme-is-global t)
     (use-package color-theme
       :ensure t)
     (color-theme-initialize)
     (load-theme 'leuven t)
     )
   )
  ;; ---------------------------------------
  ('darwin ;; MAC OS X
   (progn
     (setq mouse-wheel-scroll-amount '(2 ((shift) . 1))) ;; one line at a time
     (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
     (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
     (setq scroll-step 1) ;; keyboard scroll one line at a time     (setq scroll-step 1
     ; (arrange-frame 67 41 0 0)
     (set-face-attribute 'default nil :font "DejaVu Sans Mono-17")
     (setq color-theme-is-global t)
     (use-package color-theme
       :ensure t)
     (color-theme-initialize)
     (load-theme 'leuven t)
     )
   )
  )


; ;; sublimity
; (use-package sublimity
;   :ensure t
;   :config
;   (progn
;     (use-package sublimity-scroll
;       :config
;       (sublimity-mode 1)
;       (setq sublimity-scroll-weight 5
;             sublimity-scroll-drift-length 10)
;       )
;     )
;   )



;; pretty-mode
(use-package pretty-mode
  :ensure t
  :commands pretty-mode
  ;; :diminish pretty-mode
  :config
  (progn
    (hook-into-modes #'pretty-mode
                     '(emacs-lisp-mode-hook
                       coffee-mode-hook
                       python-mode-hook
                       ruby-mode-hook
                       haskell-mode-hook
                       latex-mode-hook
                       )))
  )


;; Line spacing
(setq-default line-spacing 5)


;; Standard indent
(setq standard-indent 2)


;; remove toolbar
(tool-bar-mode -1)


;; Do not remove menu bar
(menu-bar-mode 1)

;; turn off the annoying alarm bell
(setq ring-bell-function 'ignore)


;; ----------------------------------------------------
;; Change cursor color according to mode; inspired by
;; http://www.emacswiki.org/emacs/ChangingCursorDynamically
(setq djcb-read-only-color   "gray")
;; valid values are t, nil, box, hollow, bar, (bar . WIDTH), hbar,
;; (hbar. HEIGHT); see the docs for set-cursor-type

(setq djcb-read-only-cursor-type 'hbar)
(setq djcb-overwrite-color   "red")
(setq djcb-overwrite-cursor-type 'box)
(setq djcb-normal-color  "black")
(setq djcb-normal-cursor-type'bar)

(defun djcb-set-cursor-according-to-mode ()
  "change cursor color and type according to some minor modes."

  (cond
   (buffer-read-only
    ;; (set-cursor-color djcb-read-only-color)
    (setq cursor-type djcb-read-only-cursor-type))
   (overwrite-mode
    ;; (set-cursor-color djcb-overwrite-color)
    (setq cursor-type djcb-overwrite-cursor-type))
   (t
    ;; (set-cursor-color djcb-normal-color)
    (setq cursor-type djcb-normal-cursor-type))))

(add-hook 'post-command-hook 'djcb-set-cursor-according-to-mode)


;; --------------------------------------------
;; This is an optical wrap at the right margin
(global-visual-line-mode t)
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

                                        ; --------------------------------------------
(setq global-font-lock-mode 1) ; everything should use fonts
(setq font-lock-maximum-decoration t)



;; Tabs
(setq tab-width 2)
(setq-default indent-tabs-mode nil)



;; Set frame title to file name
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))


                                        ; Don't move the cursor when scrolling
(setq
 scroll-margin 0
 scroll-conservatively 100000
 scroll-preserve-screen-position 1)


;; don't let the cursor go into minibuffer prompt
(setq minibuffer-prompt-properties (quote (read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)))



;; =================== diminish ======================
(use-package diminish
  :ensure t
  :config
  (eval-after-load "undo-tree" '(diminish 'undo-tree-mode))
  (eval-after-load "guide-key" '(diminish 'guide-key-mode))
  (eval-after-load "smartparens" '(diminish 'smartparens-mode))
  (eval-after-load "guide-key" '(diminish 'guide-key-mode))
  (eval-after-load "eldoc" '(diminish 'eldoc-mode))
  (eval-after-load "company" '(diminish 'company-mode))
  (eval-after-load "ivy" '(diminish 'ivy-mode))
  (diminish 'visual-line-mode)
  )


;; ================= winner mode ===============
(when (fboundp 'winner-mode)
  (winner-mode 1))


;; ================= powerline =====================
(use-package powerline
  :ensure t
  :init
  (powerline-center-theme)
  )
(add-hook 'after-init-hook 'powerline-reset)



;; ========== distinguish dashes ============
;; http://emacs.stackexchange.com/questions/9623/tell-a-dash-an-en-dash-and-an-emdash-apart
;;
(let* (
       (glyph-en-dash (make-glyph-code ?\u002D 'font-lock-keyword-face))
       (glyph-em-dash (make-glyph-code ?\u002D 'font-lock-function-name-face)) )
  (when (not buffer-display-table)
    (setq buffer-display-table (make-display-table)))
  (aset buffer-display-table 8211 `[,glyph-en-dash ,glyph-en-dash])
  (aset buffer-display-table 8212 `[,glyph-em-dash ,glyph-em-dash ,glyph-em-dash]))



;; =============scratch-pop===============
(use-package scratch-pop
  :ensure t
  )

; ============= whitespace =============
(use-package whitespace :ensure t
  :init (setq highlight-indent-guides-method 'character)
  :diminish whitespace-mode
  )


;; ========= highlight-indent-guides ===================
(use-package highlight-indent-guides
  :ensure t
  :config
  (progn
    (setq highlight-indent-guides-character ?\|)
    (set-face-foreground 'highlight-indent-guides-character-face "lightgray")
  )
)




;; ============ auto-indent ==============
(use-package auto-indent-mode :ensure t
  :init (setq auto-indent-on-save-file t
              auto-indent-delete-trailing-whitespace-on-save-file t
              auto-indent-untabify-on-save-file t
              auto-indent-indent-style 'aggressive
              auto-indent-key-for-end-of-line-then-newline "<M-return>"
              auto-indent-key-for-end-of-line-insert-char-then-newline "<M-S-return>"
              )
  ; :config (auto-indent-global-mode)
  :diminish auto-indent-mode
)


(provide 'mydisplay)
